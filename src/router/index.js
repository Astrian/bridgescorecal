import Vue from 'vue'
import VueRouter from 'vue-router'
import Teams from '../views/Teams/Index.vue'
import AddTeam from '../views/Teams/AddTeam.vue'
import TeamDetail from '../views/Teams/TeamDetail.vue'
import Rounds from '../views/Rounds/Index.vue'
import AddRounds from '../views/Rounds/Add.vue'
import Desk from '../views/Rounds/Desk.vue'
import Rank from '../views/Rank/Index.vue'
import Data from '../views/Data/Index.vue'
import About from '../views/About/Index.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Rank',
    component: Rank,
    meta: {
      title: '排名'
    }
  },
  {
    path: '/teams',
    name: 'Teams',
    component: Teams,
    meta: {
      title: '队伍与牌手'
    }
  },
  {
    path: '/teams/add',
    name: 'AddTeam',
    component: AddTeam,
    meta: {
      title: '添加队伍'
    }
  },
  {
    path: '/teams/@:id',
    name: 'TeamDetail',
    component: TeamDetail,
    meta: {
      title: '队伍详情'
    }
  },
  {
    path: '/rounds',
    name: 'Rounds',
    component: Rounds,
    meta: {
      title: '轮次'
    }
  },
  {
    path: '/rounds/add',
    name: 'AddRounds',
    component: AddRounds,
    meta: {
      title: '添加轮次'
    }
  },
  {
    path: '/rounds/@:rid/desk/@:did',
    name: 'Desk',
    component: Desk,
    meta: {
      title: '牌桌详情'
    }
  },
  {
    path: '/data',
    name: 'Data',
    component: Data,
    meta: {
      title: '数据管理'
    }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: {
      title: '关于'
    }
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = `${to.meta.title} · 桥牌计分器`
  }
  next()
})

export default router
