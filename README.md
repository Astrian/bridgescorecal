# 桥牌计分器
这是一款使用 [Vue Cli](https://cli.vuejs.org/) 工具打造的一款 [定约桥牌](https://zh.wikipedia.org/zh-cn/%E5%90%88%E7%B4%84%E6%A9%8B%E7%89%8C) 计分器。

## 特性
- 支持记录队伍、牌手及对阵信息
- 支持双桌（开闭室）模式，分别登记两张牌室中的定约、定约方、IMP 等信息
- 使用 IMP（International Match Points）计算当局 VP（Victory Point）分数，并根据队伍统计总 VP
- 根据 VP 或获胜场数进行排名，并导出为 Excel 文件
- 导入 / 导出 / 分享数据
- 无需注册 / 登录，所有数据皆在本地进行处理

## 如何使用
### 直接使用
访问 [https://bridge.astrianzheng.com](https://bridge.astrianzheng.com) 即可。

### 自己部署
需要安装 `node`、`yarn`，以及 `@vue/cli`。

```
git clone git@gitlab.com:Astrian/bridgescorecal.git
cd bridgescorecal
yarn
yarn run build
```
运行完成后，`bridgescorecal/dist` 即生成的程序代码，将其部署在服务器即可。详情请查阅 Vue Cli 相关文档。

## VP 算法
![](vp-scoring.svg)

<!-- z=10\times\frac{1-(\frac{\sqrt{5}-1}{2})^{min(\frac{|M|}{5\sqrt{N}},3)}}{1-(\frac{\sqrt{5}-1}{2})^3} -->

此公式转换自世界桥牌联盟（WBF）官网公布的 *[Function for creating WBF VP Scales](http://www.worldbridge.org/wp-content/uploads/2019/06/VP-Scale-Functiondetails.pdf)*。

## 反馈与改进
请到 [本项目的议题（issue）页面](https://gitlab.com/Astrian/bridgescorecal/-/issues) 提交您的反馈。

欢迎您对代码进行改进，并提交合并请求（merge request）。

## 协议
MIT